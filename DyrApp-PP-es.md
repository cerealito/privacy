# Política de Privacidad

Al usar esta aplicación, el usuario esta sujeto a la siguiente politica de privacidad.

## Recoleccion de datos
Esta applicación **no recolecta** ningún dato personal.

## Transferencia a terceros
Esta aplicación **no transfiere** ningún dato personal a ningún tercero.

## Politica de almacenamiento y retención
Esta aplicación **no almacena** ningún dato personal por ningún periodo de tiempo.

## Cambios en la política de privacidad
La presente política de privacidad está sujeta a cambios.
El usuario de esta applicación debe revisar la nueva política de privacidad 
con cada nueva versión de la applicación.

## Contacto
El usuario puede cotnactar al autor por medio de la red social mastodon
en la siguiente dirección:
https://toot.community/@engineering

